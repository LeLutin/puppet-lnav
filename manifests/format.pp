# an "lnav format" is a (set of) regular expressions to parse logfiles
#
# @param ensure to install or remove all resources in this define
# @param content the content of the file
# @param source the source of the file
#
# All parameters are passed directly to the File resource.
define lnav::format(
  Enum['present','absent'] $ensure = 'present',
  Optional[String] $content = undef,
  Optional[String] $source  = undef,
) {
  include lnav
  file { "/etc/lnav/formats/installed/${name}.json":
    ensure  => $ensure,
    content => $content,
    source  => $source,
    require => File['/etc/lnav/formats/installed'],
  }
}
