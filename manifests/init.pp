# lnav is a log processing software
#
# @param ensure set to "absent" to purge all traces of this module
class lnav(
  Enum['present','absent'] $ensure = 'present',
) {
  if $ensure == 'present' {
    $ensure_directory = 'directory'
  } else  {
    $ensure_directory = 'absent'
  }
  package { 'lnav':
    ensure => $ensure,
  }
  file { [
    '/etc/lnav/formats/installed',
    '/etc/lnav/formats',
    '/etc/lnav',
  ]:
    ensure  => $ensure_directory,
    purge   => true,
    recurse => true,
  }
}
